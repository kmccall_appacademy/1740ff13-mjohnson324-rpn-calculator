class RPNCalculator
  def initialize
    @stack = []
  end

  def push(number)
    @stack << number
  end

  def value
    @stack.last
  end

  def plus
    perform_operation(:+)
  end

  def minus
    perform_operation(:-)
  end

  def times
    perform_operation(:*)
  end

  def divide
    perform_operation(:/)
  end

  def tokens(rpn_expression)
    tokens_to_execute = rpn_expression.split
    tokens_to_execute.map { |el| operation?(el) ? el.to_sym : el.to_i }
  end

  def evaluate(rpn_expression)
    tokens = tokens(rpn_expression)
    # can't believe how easy it was to solve this problem compared to my
    # original solution
    tokens.each do |token|
      case token
      when Integer
        push(token)
      else
        perform_operation(token)
      end
    end
    value
  end

  private
  def operation?(token)
    ["*", "/", "+", "-"].include?(token)
  end

  def perform_operation(op_symbol)
    raise "calculator is empty" if @stack.length < 2
    last_operand = @stack.pop
    first_operand = @stack.pop

    case op_symbol
    when :+
      @stack << first_operand + last_operand
    when :-
      @stack << first_operand - last_operand
    when :*
      @stack << first_operand * last_operand
    when :/
      @stack << first_operand.fdiv(last_operand)
    else
      @stack << first_operand
      @stack << last_operand
      raise "Operation #{op_symbol} not supported"
    end
  end
end
